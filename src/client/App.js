import React, { Component } from "react";
import "react-calendar/dist/Calendar.css";
import "semantic-ui-css/semantic.min.css";
import "./App.css";
import { connect } from "react-redux";
import Sidebar from "./components/Sidebar";
import Main from "./components/Main";

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="App">
        <Sidebar></Sidebar>
        <Main></Main>
        <div className="sidebar-right"></div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  //console.log(state);
  return {
    date: state.date.date,
    time: state.time.time,
  };
};

export default connect(mapStateToProps, null)(App);
