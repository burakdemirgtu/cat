import React from "react";

import TimeRangeSlider from "./TimeRangeSlider";
import CameraList from "./CameraList";
import Calendar from "./Calendar";
import GraphDropdown from "./GraphDropdown";
import CategoryDropdown from "./CategoryDropdown";
import Button from "./Button";
import UniqueVisitorCB from "./UniqueVisitorCB";

class Sidebar extends React.Component {
  render() {
    return (
      <div className="sidebar">
        <CameraList></CameraList>
        <Calendar></Calendar>
        <TimeRangeSlider></TimeRangeSlider>
        <CategoryDropdown></CategoryDropdown>
        <UniqueVisitorCB></UniqueVisitorCB>
        <GraphDropdown></GraphDropdown>
        <Button></Button>
      </div>
    );
  }
}

export default Sidebar;
