import React, { useState, useEffect } from "react";
import { Dropdown } from "semantic-ui-react";

import { connect } from "react-redux";
import { setSelectedCategory } from "../../redux/actions";

function CategoryDropdown({ categories, setSelectedCategory }) {
  //const [categoryList, setCategoryList] = useState([]);

  const onChange = (e, data) => {
    if (data.value == "allCategories") {
      setSelectedCategory(categories);
    } else {
      const selected = categories.filter(
        (category) => data.value == category[4].value
      );
      console.log(selected);
      setSelectedCategory(selected);
    }
  };

  // useEffect(() => {
  //   setCategoryList(categories.map((element) => element[4]));
  // }, []);

  return (
    <Dropdown
      placeholder="Select Category"
      selection
      options={categories.map((element) => element[4])}
      onChange={onChange}
    ></Dropdown>
  );
}

const mapStateToProps = (state) => {
  return {
    categories: state.camera.camera.categories,
  };
};

export default connect(mapStateToProps, { setSelectedCategory })(
  CategoryDropdown
);
