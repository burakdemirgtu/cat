import React, { useState } from "react";
import { Dropdown } from "semantic-ui-react";

import { connect } from "react-redux";
import { setGraph } from "../../redux/actions";

const graphList = [
  { key: 0, text: "Category Chart", value: "categorychart" },
  { key: 1, text: "Crowd Heatmap", value: "heatmap" },
  { key: 2, text: "Time Density Chart", value: "timedensitychart" },
  { key: 3, text: "Mark Series Chart", value: "markserieschart" },
];

function GraphDropdown({ setGraph }) {
  const onChange = (e, data) => {
    console.log(data);
    setGraph(data.value);
  };
  return (
    <Dropdown
      placeholder="Select Graph"
      selection
      options={graphList}
      onChange={onChange}
    ></Dropdown>
  );
}

export default connect(null, { setGraph })(GraphDropdown);
