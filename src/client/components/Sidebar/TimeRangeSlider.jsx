import React from "react";
import TimeRangeSlider from "react-time-range-slider";

import { connect } from "react-redux";
import { setRange, setTime } from "../../redux/actions";

class _TimeRangeSlider extends React.Component {
  constructor(props) {
    super(props);
    this.featureRef = React.createRef();
    this.changeStartHandler = this.changeStartHandler.bind(this);
    this.timeChangeHandler = this.timeChangeHandler.bind(this);
    this.changeCompleteHandler = this.changeCompleteHandler.bind(this);
    this.state = {
      value: {
        start: "00:00",
        end: "23:59",
      },
    };
  }

  changeStartHandler(time) {
    //console.log("Start Handler Called", time);
  }

  timeChangeHandler(time) {
    this.setState({
      value: time,
    });
    //console.log(this.state.value);
  }

  changeCompleteHandler(time) {
    this.props.setTime(time);
    const { start, end } = time;
    const min = new Date(this.props.date).setHours(start.hours, start.minutes);
    const max = new Date(this.props.date).setHours(end.hours, end.minutes);
    this.props.setRange({ min, max });
  }

  render() {
    const { value } = this.state;
    return (
      <div className="slider-container">
        <TimeRangeSlider
          disabled={false}
          format={24}
          maxValue={"23:59"}
          minValue={"00:00"}
          name={"time_range"}
          onChangeStart={this.changeStartHandler}
          onChangeComplete={this.changeCompleteHandler}
          onChange={this.timeChangeHandler}
          step={1}
          value={this.state.value}
        />
        {value.start} - {value.end}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  //console.log(state);
  return {
    date: state.date.date,
    time: state.time.time,
  };
};

export default connect(mapStateToProps, { setTime, setRange })(
  _TimeRangeSlider
);
