import React, { useState } from "react";
import { connect } from "react-redux";
import { Checkbox } from "semantic-ui-react";
import { setUniqueVisitor } from "../../redux/actions";

function UniqueVisitorCB({ uniqueVisitor, setUniqueVisitor }) {
  const toggle = () => setUniqueVisitor(!uniqueVisitor);
  return (
    <div className="checkbox-container">
      <Checkbox
        checked={uniqueVisitor}
        label="Unique Visitor"
        onChange={toggle}
      ></Checkbox>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    uniqueVisitor: state.uniqueVisitor.uniqueVisitor,
  };
};

export default connect(mapStateToProps, { setUniqueVisitor })(UniqueVisitorCB);
