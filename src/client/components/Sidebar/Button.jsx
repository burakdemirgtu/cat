import React, { useState } from "react";
import { connect } from "react-redux";
import { Button } from "semantic-ui-react";
import { setData } from "../../redux/actions";

function _Button({ date, time, setData, query }) {
  const handleClick = () => {
    console.log("handle click");
    const start = new Date(date).setHours(time.start.hours, time.start.minutes);
    const end = new Date(date).setHours(time.end.hours, time.end.minutes);
    const collectionName = query;
    console.log(start, end);
    console.log(query);
    let response = fetch(
      `http://localhost:8080/getData?start=${start}&end=${end}&collectionName=${collectionName}`
    ).then((response) => response.json());
    response.then((data) => setData(data));
  };
  return (
    <div className="button-container">
      <Button onClick={handleClick} color="green">
        Make
      </Button>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    date: state.date.date,
    time: state.time.time,
    query: state.camera.camera.query,
  };
};

export default connect(mapStateToProps, { setData })(_Button);
