import React, { useState } from "react";
import Calendar from "react-calendar";

import { connect } from "react-redux";
import { setRange, setDate } from "../../redux/actions";

function myCalendar({ setDate, setRange, time }) {
  const [value, setValue] = useState(new Date());

  const onChange = (date) => {
    setDate(date);
    setValue(date);
    const { start, end } = time;
    const min = new Date(date).setHours(start.hours, start.minutes);
    const max = new Date(date).setHours(end.hours, end.minutes);
    setRange({ min, max });
  };

  return (
    <div className="react-calendar-container">
      <Calendar onChange={onChange} value={value} />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    date: state.date.date,
    time: state.time.time,
  };
};

export default connect(mapStateToProps, { setRange, setDate })(myCalendar);
