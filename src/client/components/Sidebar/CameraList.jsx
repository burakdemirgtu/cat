import React from "react";
import { List } from "semantic-ui-react";
import { connect } from "react-redux";
import { setCamera } from "../../redux/actions";

class CameraList extends React.Component {
  handleClick = (key) => {
    console.log(key);
    const camera = this.props.cameras.filter((camera) => camera.key == key);
    console.log(camera);
    this.props.setCamera(camera[0]);
  };

  render() {
    return (
      <div className="camera-list">
        <List>
          {this.props.cameras.map((camera) => {
            return (
              <List.Item
                as="a"
                active={false}
                onClick={() => this.handleClick(camera.key)}
                key={camera.key}
              >
                <List.Icon name="video" />
                <List.Content>
                  <List.Header>{camera.name}</List.Header>
                </List.Content>
              </List.Item>
            );
          })}
        </List>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { cameras: state.cameras };
};

export default connect(mapStateToProps, { setCamera })(CameraList);
