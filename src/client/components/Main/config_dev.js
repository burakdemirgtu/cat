const config = {
  // Heatmap configuration
  HEATMAP_CANVAS_WIDTH: 950,
  HEATMAP_CANVAS_HEIGTH: 860
};

export default config;
