import React, { Component } from "react";
import { connect } from "react-redux";
import Heatmap from "./heatmap";
import BarChartForCategory from "./BarChartForCategory";
import BarChartForDensity from "./BarChartForDensity";
import MarkSeriesChart from "./MarkSeriesChart";
import config from "./config_dev";
import marketImage from "../../images/back_register.png";

class Main extends Component {
  constructor(props) {
    super(props);

    // create ref for SimpleHeat map component
    this.simpleHeatRef = React.createRef();
    // heatmap defaults
    this.defaultMaxOccurances = 18;
    this.defaultBlurValue = 10;
    this.defaultRadiusValue = 14;

    // component's state
    this.state = {
      data: [], // data array contains a list of sub-arrays with [x, y, occurances] values.  refer to data.js for example.
      maxOccurances: this.defaultMaxOccurances,
      blurValue: this.defaultBlurValue,
      radiusValue: this.defaultRadiusValue,
    };

    this.handleWindowClose = this.handleWindowClose.bind(this);
  }

  componentWillMount() {
    console.log("App.componentWillMount() props: ", this.props);
    window.addEventListener("beforeunload", this.handleWindowClose);
  }

  componentWillUnmount() {
    window.removeEventListener("beforeunload", this.handleWindowClose);
  }

  async handleWindowClose(e) {
    e.preventDefault();
    // dispatch a UNAUTH_USER action to invoke Cognito
  }

  render() {
    const { maxOccurances, blurValue, radiusValue } = this.state;
    return (
      <div className="main">
        <div className="header"></div>
        <div className="graph">
          {this.props.graph == "heatmap" && (
            <Heatmap
              ref={this.simpleHeatRef}
              width={config.HEATMAP_CANVAS_WIDTH}
              height={config.HEATMAP_CANVAS_HEIGTH}
              maxOccurances={maxOccurances}
              blur={blurValue}
              radius={radiusValue}
            ></Heatmap>
          )}
          {this.props.graph == "categorychart" && (
            <BarChartForCategory></BarChartForCategory>
          )}
          {this.props.graph == "timedensitychart" && (
            <BarChartForDensity></BarChartForDensity>
          )}
          {this.props.graph == "markserieschart" && (
            <MarkSeriesChart></MarkSeriesChart>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    graph: state.graph.graph,
  };
};

export default connect(mapStateToProps, null)(Main);
