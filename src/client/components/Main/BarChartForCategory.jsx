import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  XYPlot,
  XAxis,
  YAxis,
  VerticalGridLines,
  HorizontalGridLines,
  VerticalBarSeries,
  FlexibleXYPlot,
} from "react-vis";
const sourceWidth = 965;
const sourceHeight = 722;

const getResult = (locations, categories) => {
  var categoryData = categories.map((element) => {
    console.log(element);
    return { x: element[4].text, y: 0 };
  });
  categoryData.shift();

  locations.forEach((element) => {
    let location = {
      x: ((element.bound_boxes[0] + element.bound_boxes[2]) / 2) * sourceWidth,
      y: ((element.bound_boxes[1] + element.bound_boxes[3]) / 2) * sourceHeight,
    };
    //console.log(location);
    var i = inWhichCategory(location, categories);
    //console.log(i);
    if (i != -1) categoryData[i].y++;
  });
  console.log(categoryData);
  return categoryData;
};

const inWhichCategory = (location, categories) => {
  //0 index is total area
  for (let i = 1; i < categories.length; i++) {
    if (pointIsInPoly(location, categories[i])) return i - 1;
  }
  return -1;
};

//https://stackoverflow.com/questions/217578/how-can-i-determine-whether-a-2d-point-is-within-a-polygon
function pointIsInPoly(p, category) {
  var polygon = category.slice(0, 4);
  var isInside = false;
  var minX = polygon[0].x,
    maxX = polygon[0].x;
  var minY = polygon[0].y,
    maxY = polygon[0].y;
  for (var n = 1; n < polygon.length; n++) {
    var q = polygon[n];
    minX = Math.min(q.x, minX);
    maxX = Math.max(q.x, maxX);
    minY = Math.min(q.y, minY);
    maxY = Math.max(q.y, maxY);
  }

  if (p.x < minX || p.x > maxX || p.y < minY || p.y > maxY) {
    return false;
  }

  var i = 0,
    j = polygon.length - 1;
  for (i, j; i < polygon.length; j = i++) {
    if (
      polygon[i].y > p.y != polygon[j].y > p.y &&
      p.x <
        ((polygon[j].x - polygon[i].x) * (p.y - polygon[i].y)) /
          (polygon[j].y - polygon[i].y) +
          polygon[i].x
    ) {
      isInside = !isInside;
    }
  }

  return isInside;
}

function BarChartForCategory({ data, categories }) {
  const [categoryData, setCategoryData] = useState([
    { x: "vegetables", y: 0 },
    { x: "snacks", y: 0 },
    { x: "charcuterie", y: 0 },
    { x: "fruits", y: 0 },
    { x: "driedGoods", y: 0 },
    { x: "freezer", y: 0 },
  ]);

  useEffect(() => {
    setCategoryData(getResult(data, categories));
  }, [data]);

  return (
    <FlexibleXYPlot margin={{ bottom: 70 }} xType="ordinal">
      <VerticalGridLines />
      <HorizontalGridLines />
      <XAxis tickLabelAngle={-45} />
      <YAxis />
      <VerticalBarSeries data={categoryData} />
    </FlexibleXYPlot>
  );
}

const mapStateToProps = (state) => {
  return {
    data: state.data.data,
    categories: state.camera.camera.categories,
    uniqueVisitor: state.uniqueVisitor.uniqueVisitor,
  };
};

export default connect(mapStateToProps)(BarChartForCategory);
