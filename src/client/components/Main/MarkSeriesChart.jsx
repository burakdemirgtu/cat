import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { setData } from "../../redux/actions";
import { MarkSeries, FlexibleXYPlot, Hint, XAxis, YAxis } from "react-vis";

const tipStyle = {
  display: "flex",
  color: "#fff",
  background: "#000",
  alignItems: "center",
  padding: "5px",
};

function reshapeData(data) {
  return Object.values(
    data.reduce((a, { tracking_id, bound_boxes, time }) => {
      if (!a[tracking_id])
        a[tracking_id] = { tracking_id, bound_boxes: [], time };
      a[tracking_id].bound_boxes.push({
        x: bound_boxes[0] / 2 + bound_boxes[2] / 2,
        y: bound_boxes[3],
        tracking_id: tracking_id,
      });
      a[tracking_id].timeEnd = time;
      return a;
    }, {})
  );
}

function MarkSeriesChart({ data }) {
  const [hoveredCell, setHoveredCell] = useState(false);
  const [dataSeries, setDataSeries] = useState([]);

  useEffect(() => {
    console.log(data.length);
    console.log(reshapeData(data).length);
    setDataSeries(reshapeData(data));
  }, [data]);

  return (
    <FlexibleXYPlot margin={{ bottom: 70 }} xDomain={[0, 1]} yDomain={[1, 0]}>
      {dataSeries.map((serie) => {
        return (
          <MarkSeries
            key={serie.tracking_id}
            data={serie.bound_boxes}
            onValueMouseOver={(v) => {
              setHoveredCell(v);
              console.log(v);
            }}
            onValueMouseOut={(v) => setHoveredCell(false)}
          ></MarkSeries>
        );
      })}

      {hoveredCell && (
        <Hint value={hoveredCell}>
          <div style={tipStyle}>Tracking ID: {hoveredCell.tracking_id}</div>
        </Hint>
      )}
      <XAxis title="X" />
      <YAxis />
    </FlexibleXYPlot>
  );
}

const mapStateToProps = (state) => {
  return {
    data: state.data.data,
  };
};

export default connect(mapStateToProps, { setData })(MarkSeriesChart);
