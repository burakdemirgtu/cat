import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { setData } from "../../redux/actions";
import {
  XYPlot,
  XAxis,
  YAxis,
  VerticalGridLines,
  HorizontalGridLines,
  VerticalBarSeries,
  FlexibleXYPlot,
  LineMarkSeries,
  LineSeries,
} from "react-vis";
import { pointIsInPoly } from "../../common";

const getResult = (locations, times) => {
  console.log(times);
  locations.forEach((element) => {
    var i = inWhichTime(element.time, times);
    //console.log(i);
    if (i != -1) times[i].y++;
  });

  return times;
};

const inWhichTime = (time, times) => {
  for (let index = times.length - 1; index >= 0; index--) {
    if (times[index].x <= time) return index;
  }
  return -1;
};

function reshapeData(data) {
  return Object.values(
    data.reduce((a, { tracking_id, bound_boxes, time }) => {
      if (!a[tracking_id])
        a[tracking_id] = { tracking_id, bound_boxes: [], time };
      a[tracking_id].bound_boxes.push({
        x: bound_boxes[0] / 2 + bound_boxes[2] / 2,
        y: bound_boxes[1] / 2 + bound_boxes[3] / 2,
        tracking_id: tracking_id,
      });
      a[tracking_id].timeEnd = time;
      return a;
    }, {})
  );
}

const getUniqueResult = (locations, times) => {
  console.log(reshapeData(locations));
  reshapeData(locations).forEach((element) => {
    var i = inWhichTime(element.time, times);
    var iEnd = inWhichTime(element.timeEnd, times);

    //console.log(i, iEnd);
    if (i != -1) {
      for (let index = i; index <= iEnd; index++) {
        times[index].y++;
      }
    }
  });
  return times;
};

const getHH_MMFormat = (ms) => {
  var hour = new Date(ms).getHours();
  var minute = new Date(ms).getMinutes();
  hour = hour == 0 ? "00" : hour;
  hour = hour < 10 && hour > 0 ? "0" + hour : hour;
  minute = minute == 0 ? "00" : minute;
  minute = minute < 10 && minute > 0 ? "0" + minute : minute;
  return `${hour}:${minute}`;
};

const sourceWidth = 965;
const sourceHeight = 722;

const filterByCategory = (data, selectedCategory) => {
  console.log(data.length);
  console.log(selectedCategory);
  if (data.length == 0) return [];
  let filteredData = data.filter((element) => {
    let location = {
      x: ((element.bound_boxes[0] + element.bound_boxes[2]) / 2) * sourceWidth,
      y: ((element.bound_boxes[1] + element.bound_boxes[3]) / 2) * sourceHeight,
    };
    // console.log(location);
    return pointIsInPoly(location, selectedCategory[0]);
  });
  console.log(filteredData.length);
  return filteredData;
};

function BarChartForDensity({ data, uniqueVisitor, selectedCategory }) {
  const [times, setTimes] = useState([]);

  useEffect(() => {
    if (selectedCategory.length == 1) {
      data = filterByCategory(data, selectedCategory);
    }
    if (data.length == 0) return;
    let slice = (data[data.length - 1].time - data[0].time) / 10;
    let array = new Array(10);
    for (let index = 0; index < array.length; index++) {
      array[index] = { x: data[0].time + index * slice, y: 0 };
    }

    setTimes(
      uniqueVisitor
        ? getUniqueResult(data, array).map((element, i) => {
            var hh_mm = getHH_MMFormat(element.x * 1000);
            return { x: hh_mm, y: element.y };
          })
        : getResult(data, array).map((element, i) => {
            var hh_mm = getHH_MMFormat(element.x * 1000);
            return { x: hh_mm, y: element.y };
          })
    );
  }, [data]);

  return (
    <FlexibleXYPlot margin={{ bottom: 70 }} xType="ordinal">
      <VerticalGridLines />
      <HorizontalGridLines />
      <XAxis tickLabelAngle={-45} />
      <YAxis />
      <LineSeries data={times} curve="curveBasis" />
    </FlexibleXYPlot>
  );
}

const mapStateToProps = (state) => {
  return {
    data: state.data.data,
    uniqueVisitor: state.uniqueVisitor.uniqueVisitor,
    selectedCategory: state.selectedCategory.selectedCategory,
  };
};

export default connect(mapStateToProps, { setData })(BarChartForDensity);
