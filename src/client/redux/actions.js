import {
  SET_DATE,
  SET_TIME,
  ADD_DATA,
  SET_RANGE,
  SET_GRAPH,
  SET_DATA,
  SET_UNIQUE_VISITOR,
  SET_CATEGORY,
  SET_CAMERA,
  SET_CAMERAS,
  SET_SELECTED_CATEGORY,
} from "./actionTypes";

export const setDate = (date) => ({
  type: SET_DATE,
  payload: date,
});

export const setTime = (time) => ({
  type: SET_TIME,
  payload: time,
});

export const addData = (data) => ({
  type: ADD_DATA,
  payload: data,
});

export const setRange = (range) => ({
  type: SET_RANGE,
  payload: range,
});

export const setGraph = (graph) => ({
  type: SET_GRAPH,
  payload: graph,
});

export const setData = (data) => ({
  type: SET_DATA,
  payload: data,
});

export const setUniqueVisitor = (data) => ({
  type: SET_UNIQUE_VISITOR,
  payload: data,
});

export const setCategory = (data) => ({
  type: SET_CATEGORY,
  payload: data,
});

export const setCamera = (data) => ({
  type: SET_CAMERA,
  payload: data,
});

export const setCameras = (data) => ({
  type: SET_CAMERAS,
  payload: data,
});

export const setSelectedCategory = (data) => ({
  type: SET_SELECTED_CATEGORY,
  payload: data,
});
