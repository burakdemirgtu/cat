import { SET_TIME } from "../actionTypes";

const initialState = {
  time: {
    start: {
      hours: 0,
      minutes: 0,
      am_pm: null,
    },
    end: {
      hours: 23,
      minutes: 59,
      am_pm: null,
    },
  },
};

const setTimeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_TIME: {
      const time = action.payload;
      return {
        ...state,
        time,
      };
    }
    default: {
      return state;
    }
  }
};

export default setTimeReducer;
