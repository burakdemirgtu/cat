import { SET_GRAPH } from "../actionTypes";

const initialState = {
  graph: "heatmap",
};
const setGraphReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_GRAPH: {
      const graph = action.payload;
      return {
        ...state,
        graph,
      };
    }
    default: {
      return state;
    }
  }
};

export default setGraphReducer;
