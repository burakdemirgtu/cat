import { SET_SELECTED_CATEGORY } from "../actionTypes";

const initialState = [];
const setSelectedCategory = (state = initialState, action) => {
  switch (action.type) {
    case SET_SELECTED_CATEGORY: {
      const selectedCategory = action.payload;
      return {
        ...state,
        selectedCategory,
      };
    }
    default: {
      return state;
    }
  }
};

export default setSelectedCategory;
