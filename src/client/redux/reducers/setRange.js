import { SET_RANGE } from "../actionTypes";

const initialState = {
  range: { min: 0, max: Date.now() },
};
const setRangeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_RANGE: {
      const range = action.payload;
      return {
        ...state,
        range,
      };
    }
    default: {
      return state;
    }
  }
};

export default setRangeReducer;
