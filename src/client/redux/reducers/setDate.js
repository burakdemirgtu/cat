import { SET_DATE } from "../actionTypes";

const initialState = { date: new Date().setHours(0, 0, 0, 0) };
const setDateReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DATE: {
      const date = action.payload;

      return {
        ...state,
        date,
      };
    }
    default: {
      return state;
    }
  }
};

export default setDateReducer;
