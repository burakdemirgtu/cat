import { ADD_DATA } from "../actionTypes";

const initialState = {
  data: [
    [496, 767, 1608840961771],
    [315, 430, 1608840961977],
    [551, 352, 1608840962179],
    [98, 368, 1608840962383],
    [491, 417, 1608840962585],
    [276, 130, 1608840962790],
    [404, 123, 1608840962996],
    [232, 203, 1608840963204],
    [583, 80, 1608840963409],
    [434, 523, 1608840963616],
    [700, 607, 1608840963823],
  ],
};
const addDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_DATA: {
      const data = action.payload;
      let oldData = state.data;
      oldData.push(data);
      return {
        ...state,
        data: oldData,
      };
    }
    default: {
      return state;
    }
  }
};

export default addDataReducer;
