import { SET_UNIQUE_VISITOR } from "../actionTypes";

const initialState = {
  uniqueVisitor: true,
};
const setUniqueVisitor = (state = initialState, action) => {
  switch (action.type) {
    case SET_UNIQUE_VISITOR: {
      return {
        ...state,
        uniqueVisitor: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};

export default setUniqueVisitor;
