import { SET_CAMERAS } from "../actionTypes";

const initialState = [
  {
    name: "Camera 1",
    key: 0,
    query: "datas",
    sourceWidth: 965,
    sourceHeight: 722,
    image: "../../images/market.png",
    storeName: "Store A",
    categories: [
      [
        { x: 0, y: 0 },
        { x: 0, y: 722 },
        { x: 965, y: 722 },
        { x: 965, y: 0 },
        { key: 0, text: "All Categories", value: "allCategories" },
      ],
      [
        { x: 158, y: 263 },
        { x: 963, y: 664 },
        { x: 349, y: 720 },
        { x: 0, y: 369 },
        { key: 1, text: "Freezer", value: "freezer" },
      ],
      [
        { x: 457, y: 407 },
        { x: 964, y: 663 },
        { x: 963, y: 296 },
        { x: 718, y: 237 },
        { key: 2, text: "Vegetables I", value: "vegetablesI" },
      ],
      [
        { x: 457, y: 407 },
        { x: 270, y: 320 },
        { x: 568, y: 190 },
        { x: 718, y: 237 },
        { key: 3, text: "Vegetables II", value: "vegetablesII" },
      ],
      [
        { x: 963, y: 296 },
        { x: 869, y: 168 },
        { x: 964, y: 209 },
        { x: 718, y: 237 },
        { key: 4, text: "Vegetables III", value: "vegetablesIII" },
      ],
      [
        { x: 568, y: 190 },
        { x: 869, y: 168 },
        { x: 707, y: 127 },
        { x: 718, y: 237 },
        { key: 5, text: "Vegetables IV", value: "vegetablesIV" },
      ],
      [
        { x: 568, y: 190 },
        { x: 270, y: 320 },
        { x: 324, y: 125 },
        { x: 41, y: 237 },
        { key: 6, text: "Fruits", value: "fruits" },
      ],
    ],
  },
  {
    name: "Camera 2",
    key: 1,
    query: "StoreACamera2",
    sourceWidth: 640,
    sourceHeight: 480,
    image: "../../images/market.png",
    storeName: "Store A",
    categories: [
      [
        { x: 0, y: 0 },
        { x: 0, y: 480 },
        { x: 640, y: 0 },
        { x: 640, y: 480 },
        { key: 7, text: "All Categories", value: "allCategories" },
        [
          { x: 290, y: 480 },
          { x: 640, y: 480 },
          { x: 320, y: 190 },
          { x: 550, y: 190 },
          { key: 13, text: "Discount Bins", value: "discountBins" },
        ],
        [
          { x: 320, y: 190 },
          { x: 550, y: 190 },
          { x: 350, y: 50 },
          { x: 420, y: 50 },
          { key: 14, text: "Discount Bins II", value: "discountBinsII" },
        ],
        [
          { x: 320, y: 190 },
          { x: 350, y: 50 },
          { x: 300, y: 50 },
          { x: 170, y: 170 },
          { key: 15, text: "Butcher", value: "butcher" },
        ],
        [
          { x: 320, y: 190 },
          { x: 170, y: 170 },
          { x: 290, y: 480 },
          { x: 1, y: 440 },
          { key: 16, text: "Sea Food", value: "seaFood" },
        ],
      ],
    ],
  },
  {
    name: "Camera 1",
    key: 2,
    query: "StorBCamera1",
    sourceWidth: 640,
    sourceHeight: 480,
    image: "../../images/market.png",
    storeName: "Store B",
    categories: [
      [
        { x: 0, y: 0 },
        { x: 0, y: 480 },
        { x: 640, y: 0 },
        { x: 640, y: 480 },
        { key: 12, text: "All Categories", value: "allCategories" },
      ],
      [
        { x: 30, y: 480 },
        { x: 245, y: 480 },
        { x: 245, y: 245 },
        { x: 95, y: 245 },
        { key: 8, text: "Snacks", value: "snacks" },
      ],
      [
        { x: 95, y: 245 },
        { x: 245, y: 245 },
        { x: 240, y: 70 },
        { x: 150, y: 70 },
        { key: 9, text: "Drinks", value: "drinks" },
      ],
      [
        { x: 245, y: 480 },
        { x: 600, y: 480 },
        { x: 245, y: 245 },
        { x: 436, y: 245 },
        { key: 10, text: "Cashier I", value: "cashierI" },
      ],
      [
        { x: 436, y: 245 },
        { x: 245, y: 245 },
        { x: 245, y: 0 },
        { x: 300, y: 0 },
        { key: 11, text: "Cashier II", value: "cashierII" },
      ],
    ],
  },
];
const setCamerasReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CAMERAS: {
      return {
        ...state,
        cameras: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};

export default setCamerasReducer;
