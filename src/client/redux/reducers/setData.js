import { SET_DATA } from "../actionTypes";

const initialState = {
  data: [],
};
const setDataReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DATA: {
      return {
        ...state,
        data: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};

export default setDataReducer;
