import { SET_CAMERA } from "../actionTypes";

const initialState = {
  camera: {
    name: "Camera 1",
    key: 0,
    query: "datas",
    sourceWidth: 965,
    sourceHeight: 722,
    image: "../../images/market.png",
    storeName: "Store A",
    categories: [
      [
        { x: 0, y: 0 },
        { x: 0, y: 722 },
        { x: 965, y: 722 },
        { x: 965, y: 0 },
        { key: 0, text: "All Categories", value: "allCategories" },
      ],
      [
        { x: 158, y: 263 },
        { x: 963, y: 664 },
        { x: 349, y: 720 },
        { x: 0, y: 369 },
        { key: 1, text: "Freezer", value: "freezer" },
      ],
      [
        { x: 457, y: 407 },
        { x: 964, y: 663 },
        { x: 963, y: 296 },
        { x: 718, y: 237 },
        { key: 2, text: "Vegetables I", value: "vegetablesI" },
      ],
      [
        { x: 457, y: 407 },
        { x: 270, y: 320 },
        { x: 568, y: 190 },
        { x: 718, y: 237 },
        { key: 3, text: "Vegetables II", value: "vegetablesII" },
      ],
      [
        { x: 963, y: 296 },
        { x: 869, y: 168 },
        { x: 964, y: 209 },
        { x: 718, y: 237 },
        { key: 4, text: "Vegetables III", value: "vegetablesIII" },
      ],
      [
        { x: 568, y: 190 },
        { x: 869, y: 168 },
        { x: 707, y: 127 },
        { x: 718, y: 237 },
        { key: 5, text: "Vegetables IV", value: "vegetablesIV" },
      ],
      [
        { x: 568, y: 190 },
        { x: 270, y: 320 },
        { x: 324, y: 125 },
        { x: 41, y: 237 },
        { key: 6, text: "Fruits", value: "fruits" },
      ],
    ],
  },
};

const setCameraReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CAMERA: {
      return {
        ...state,
        camera: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};

export default setCameraReducer;
