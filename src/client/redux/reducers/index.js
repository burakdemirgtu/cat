import { combineReducers } from "redux";
import date from "./setDate";
import time from "./setTime";
//import data from "./addData";
import range from "./setRange";
import graph from "./setGraph";
import data from "./setData";
import uniqueVisitor from "./setUniqueVisitor";
import categories from "./setCategory";
import camera from "./setCamera";
import cameras from "./setCameras";
import selectedCategory from "./setSelectedCategory";

export default combineReducers({
  date,
  time,
  data,
  range,
  graph,
  uniqueVisitor,
  categories,
  camera,
  cameras,
  selectedCategory,
});
