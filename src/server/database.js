const MongoClient = require("mongodb").MongoClient;

const url = "mongodb://localhost:27017";
const dbName = "CAT";
var client = new MongoClient(url, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
});

module.exports = async function getCollection(collection = "datas") {
  client = await client.connect().catch((err) => console.log(err));
  const db = client.db(dbName);
  return db.collection(collection);
};

// const insertDocuments = function (db, callback) {
//   // Get the documents collection
//   const collection = db.collection("documents");
//   // Insert some documents
//   collection.insert([{ a: 1 }, { a: 2 }, { a: 3 }], function (err, result) {
//     console.log("Inserted 3 documents into the collection");
//     callback(result);
//   });
// };
