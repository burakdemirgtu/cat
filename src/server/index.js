const express = require("express");
var cors = require("cors");
const http = require("http");
const socketIo = require("socket.io");
const port = 8080;
const app = express();
app.use(express.static("dist"));
app.use(cors());
const server = http.createServer(app);
const io = socketIo(server);

//require("./receiver");
const getCollection = require("./database");
var collection = getCollection("StoreBCamera1");

io.on("connection", (socket) => {
  console.log("New client connected" + socket.id);
  socket.on("disconnect", () => {
    console.log("Client disconnected");
  });
});

app.get("/getData", (req, res) => {
  const start = parseInt(req.query.start);
  const end = parseInt(req.query.end);
  const collectionName = req.query.collectionName;
  console.log(typeof start, typeof end);
  getCollection(collectionName).then((collection) =>
    collection
      .find({ time: { $gte: start / 1000, $lte: end / 1000 } })
      .project({ _id: 0 })
      .toArray(function (err, result) {
        if (err) throw err;
        //console.log(result);

        // let groupedResult = Object.values(
        //   result.reduce((a, { tracking_id, bound_boxes, time }) => {
        //     if (!a[tracking_id])
        //       a[tracking_id] = { tracking_id, bound_boxes: [], time };
        //     a[tracking_id].bound_boxes.push([...bound_boxes]);
        //     a[tracking_id].time = time * 1000;
        //     return a;
        //   }, {})
        // );
        res.status(200).json(result);
      })
  );
});

server.listen(port, () => console.log(`Listening on port ${port}`));
