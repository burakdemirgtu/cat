var amqp = require("amqplib/callback_api");
const getCollection = require("./database");

var collection = getCollection("datas");

amqp.connect("amqp://root:root@192.168.0.18", function (error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function (error1, channel) {
    if (error1) {
      throw error1;
    }

    var queue = "data";

    channel.assertQueue(queue, {
      durable: false,
    });

    //console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

    collection.then((collection) => {
      channel.consume(
        queue,
        function (msg) {
          data = JSON.parse(msg.content.toString());
          collection.insertOne(data);
          console.log(data);
        },
        {
          noAck: true,
        }
      );
    });
  });
});
