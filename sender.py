import pika
import threading
import random
from datetime import datetime

def generate_data():
    x = random.randint(0,950)
    y = random.randint(0,850)
    time = int(datetime.now().timestamp() * 1000)
    channel.basic_publish(exchange='', routing_key='data', body=str(x) + " " + str(y) + " " + str(time))

def set_interval(func, sec):
    def func_wrapper():
        set_interval(func, sec)
        func()
    t = threading.Timer(sec, func_wrapper)
    t.start()
    return t

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='data')

set_interval(generate_data, 0.5)




